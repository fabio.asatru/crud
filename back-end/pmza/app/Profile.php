<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $fillable = [
        'name', 'email', 'password', 'tempo_expiracao_senha','login', 'cod_autorizacao', 'status_usuario', 'cod_pessoa'];

}
