<?php

namespace App\Http\Controllers;


use App\Service\UserService;
use App\User;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Http\Request;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {

        $this->userService = $userService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->userService->listar();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->userService->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->userService->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       return  $this->userService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = new User();
        $user =  $user->find($id);
        $user->delete();
    }


    /**
     * restaurando usuario
     *
     * @param $id
     */
    public function restore($id)
    {
        $user = new User();
        $user =  $user->find($id);
        $user->restore();
    }


    public function relatorio()
    {

        $pdf= new FPDF("P","pt","A4");
        $pdf->AddPage();

        $pdf->SetFont('arial','B',18);
        $pdf->Cell(0,5,"Relatorio de Usuarios",0,1,'C');
        $pdf->Cell(0,5,"","B",1,'C');
        $pdf->Ln(50);

        //cabeçalho da tabela
        $pdf->SetFont('arial','B',14);
        $pdf->Cell(130,20,'Nome ',1,0,"L");
        $pdf->Cell(160,20,'Email',1,0,"L");
        $pdf->Cell(120,20,'Codigo Pessoa',1,0,"L");
        $pdf->Cell(120,20,'Status',1,1,"L");

        //linhas da tabela
        $pdf->SetFont('arial','',12);

        $users = $this->userService->listar();
        foreach ($users as $user){
            $pdf->Cell(130,20,$user->name,1,0,"L");
            $pdf->Cell(160,20,$user->email,1,0,"L");
            $pdf->Cell(120,20,$user->cod_pessoa,1,0,"L");
            $pdf->Cell(120,20,$user->status_usuario == 'a' ? 'ativo' : 'inativo',1,1,"L");
        }


        $pdf->Output('file.pdf','F');
        return response()->file('file.pdf');

    }

}
