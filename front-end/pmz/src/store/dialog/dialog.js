export const dialog = {
    state: {
        dialogStatus: false,
        dialogTitle: "",
        dialogDesfazer: false
    },
    namespaced: true,
    mutations: {
        setDialogStatus(state, status){
            state.dialogStatus = status
        },
        setDialogTitle(state, title){
            state.dialogTitle = title
        },
        setDialogDesfazer(state, status){
            state.dialogDesfazer = status
        }
    },
    getters: {
        dialogStatus: state => {
            return state.dialogStatus;
        },
        dialogTitle: state => {
            return state.dialogTitle;
        },
        dialogDesfazer: state => {
            return state.dialogDesfazer;
        }
    },
    actions: {
        showDialog(context, payload) {
            context.commit('setDialogStatus', payload.dialogStatus);
            context.commit('setDialogTitle', payload.dialogTitle);
            context.commit('setDialogDesfazer', payload.dialogDesfazer);
        },
        resetDialog(context){
            context.commit('setDialogStatus', false);
            context.commit('setDialogTitle', "");
            context.commit('setDialogDesfazer', false);
        }
    }
}