<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23/07/19
 * Time: 18:57
 */

namespace App\Service;


use App\Profile;

class ProfileService
{

    private $profile;


    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }

    public function list()
    {
        return $this->profile->get();
    }

}
