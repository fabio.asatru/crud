export const user = {
    state: {
        users: []
    },
    namespaced: true,
    mutations: {
        addUsers(state, user) {
            let users = state.users;
            users.push(...user);
        },
    },
    getters: {
        users: state => {
            return state.users;
        }
    },
    actions: {
        getUsers(context) {
            this._vm.axios.get(`${process.env.VUE_APP_ROOT_API}users`)
                .then((response) => {
                    context.state.users = [];
                    let users = response.data;
                    context.commit('addUsers', users);
                });
        },
        addUsers(context, users) {

            users.status_usuario = users.status_usuario ? 'a' : 'i';  

            this._vm.axios.post(`${process.env.VUE_APP_ROOT_API}users`, users).then(response => {
                let user = response;
                context.dispatch('getUsers');
                this.dispatch('dialog/showDialog', {
                    dialogStatus: true,
                    dialogTitle: "Usuário Cadastrado Com Sucesso !",
                    dialogDesfazer: false
                })
            }).catch(error => {
                context.dispatch('dialog/showDialog', {
                    dialogStatus: true,
                    dialogTitle: "Nāo Foi Possivel Cadastrar Usuário !",
                    dialogDesfazer: false
                })
            }) 
        },
        updateUser(context, {user, id}){

            user.status_usuario = user.status_usuario ? 'a' : 'i';  

            this._vm.axios.put(`${process.env.VUE_APP_ROOT_API}users/${id}`, user).then(response => {
                let user = response;
                context.dispatch('getUsers');
                this.dispatch('dialog/showDialog', {
                    dialogStatus: true,
                    dialogTitle: "Usuário Atualizado Com Sucesso !",
                    dialogDesfazer: false
                })
            }).catch(error => {
                context.dispatch('dialog/showDialog', {
                    dialogStatus: true,
                    dialogTitle: "Nāo Foi Possivel Atualizar Usuário !",
                    dialogDesfazer: false
                })
            }) 
        },
        deleteUser(context, id){
            this._vm.axios.delete(`${process.env.VUE_APP_ROOT_API}users/${id}`).then(response => {
                context.dispatch('getUsers');
                this.dispatch('dialog/showDialog', {
                    dialogStatus: true,
                    dialogTitle: "Usuário Deleteado com sucesso !",
                    dialogDesfazer: false
                })
            }).catch(error => {
                context.dispatch('dialog/showDialog', {
                    dialogStatus: true,
                    dialogTitle: "Nāo Foi Possivel Deletar Usuário !",
                    dialogDesfazer: false
                })
            }) 
        }
    }
}