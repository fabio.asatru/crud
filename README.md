# PROJETO CRUD LARAVEL - VUE

## Instruções de Uso Laravel
```
    Criar um banco com o mesmo nome do .env-example do projeto laravel
    e rodar migration e seed 
    ex: php artisan migrate --seed
```

### Instruções de Uso Vue
```
    Para acessar a api, basta setar o endpoint escolhido no .env do projeto
    e rodar npm run serve
```



