import Vue from 'vue';
import Vuex from 'vuex';
// import {user} from './user/user';

import {user} from './user/user'
import {userForm} from './userForm/userForm'
import {dialog} from './dialog/dialog'

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    user,
    userForm,
    dialog
  }
});
