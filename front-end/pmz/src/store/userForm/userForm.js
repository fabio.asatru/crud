export const userForm = {
    state: {
        visible: false,
        title: "",
        selectedUser: null
    },
    namespaced: true,
    mutations: {
        setUser(state, user) {
            state.selectedUser = user;
        },
        setVisible(state, visible) {
            state.visible = visible;
        },
        setTitle(state, title) {
            state.title = title
        }
    },
    getters: {
        getTitle: state => {
            return state.title;
        },
        getVisible: state => {
            return state.visible;
        },
        getSelectedUser: state => {
            return state.selectedUser
        }
    },
    actions: {
        selectUser(context, id){
            this._vm.axios.get(`${process.env.VUE_APP_ROOT_API}users/${id}`)
            .then((response) => {
                let user = response.data;
                context.commit('setUser', user);
            })
        },
        setVisible(context, status) {
            context.commit('setVisible', status);
        },
        setTitle(context, title) {
            context.commit('setTitle', title);
        },
        resetSelectedUser(context){
            context.state.selectedUser = null;
        }
    }
}