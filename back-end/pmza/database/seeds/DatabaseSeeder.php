<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('gadgets')->insert([
            'gadget_description' => 'gadget 1',
            'gadget_code' => '123'
        ]);

        DB::table('gadgets')->insert([
            'gadget_description' => 'gadget 2',
            'gadget_code' => '321'
        ]);

        DB::table('profiles')->insert([
            'name_profile' => 'profile 1',
        ]);


        DB::table('profiles')->insert([
            'name_profile' => 'profile 2',
        ]);

        for ($i = 0; $i < 10; $i++){
            DB::table('users')->insert([
                'name' => Str::random(10).$i,
                'email' => Str::random(10).$i.'@gmail.com',
                'login' => Str::random(10),
                'tempo_expiracao_senha' => 20,
                'cod_autorizacao' => 'A',
                'status_usuario' => 'a',
                'cod_pessoa' => '289729',
                'password' => bcrypt('password'),
            ]);
        }




    }
}
